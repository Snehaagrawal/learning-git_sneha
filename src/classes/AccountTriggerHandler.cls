public class AccountTriggerHandler{

private static AccountTriggerHandler instance;
private  Map<String,Account> newAccMap  = new Map<String,Account>();

public static AccountTriggerHandler getInstance(){
    if(instance==null){
        instance = new AccountTriggerHandler ();
    }
    return instance;
}

public void beforeInsert(List<Account> newList){
    accountTriggerHelperMethod(newList);
    
}

public void beforeUpdate(List<Account> newList){
    accountTriggerHelperMethod(newList);
}

private void accountTriggerHelperMethod(List<Account> newList){
     for(Account accRec :newList){
       if(!newAccMap.containsKey(accRec.Name)){
           newAccMap.put(accRec.Name,accRec);
       }
       else{
           accRec.Name.addError('Duplicate Name');
       }
   }
   for(Account a : [Select id,name from Account where Name IN:newAccMap.keySet()]){      
       newAccMap.get(a.Name).Name.addError('Name Already Used');
   }
 }
}