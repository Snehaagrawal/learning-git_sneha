public with sharing class StudentTriggerHandler {
	
	
	private static StudentTriggerHandler instance;
	public List<Student__c> studentList=new List<Student__c>();
    public static StudentTriggerHandler getInstance()
    {
        if (instance == null) instance = new StudentTriggerHandler();
        return instance;
    }
 
 	public void OnbeforeInsert(Student__c[] newObjects){       
                    
            for(Student__c student : newObjects)
            {
                if(student__c.City__c==null)
                student.Percent__c.addError('Please enter City');
              	studentList.add(student);
             }
         
          insert studentList;
      }
}