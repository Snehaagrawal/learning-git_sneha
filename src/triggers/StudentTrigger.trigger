/*************************************************************************************************
Created By: Sneha Agrawal
Created Date: 8/28/14
Version: 31.0
Description: Trigger on Student
**************************************************************************************************/
trigger StudentTrigger on Student__c (before insert) {
	
	StudentTriggerHandler handler = StudentTriggerHandler.getInstance();
	
     /********* before insert  ********/
     
	if(Trigger.isBefore && Trigger.isInsert)
	{
		 handler.OnBeforeInsert (Trigger.new); 
	}
	

}